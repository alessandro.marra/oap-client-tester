﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace OapClient
{
    class XmlGenerator
    {
        private static string OapVersion = "4.0";
        private static string ServiceVersion = "1.0";

        private static int counterXmlId = 0;
        private static string idHead = "1000";
        //private static int counterMessageId = 0;

        public static String GenerateXmlId()
        {
            string value = idHead;
            if (idHead == "1000")
            {
                value = String.Format("{0:X4}", (new Random()).Next(0x10000));
                idHead = value;
            }
            counterXmlId++;
            var id = value + counterXmlId.ToString().PadLeft(4, '0');
            return id.ToLower();
        }

        //private static String GenerateRequestId()
        //{
        //    var random = new Random();
        //    var value = String.Format("{0:X4}", random.Next(0x10000));
        //    counterMessageId++;
        //    var id = value + counterMessageId.ToString().PadLeft(4, '0');
        //    return id.ToLower();
        //}

        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }

        public static String AddClient(int port, int time)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString())),
                    new XElement("Body",
                    new XAttribute("service", "addClient"),
                        new XElement("UpdTime", time.ToString())
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String Supervision(int port)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString())),
                    new XElement("Body",
                    new XAttribute("service", "supervision")
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String ResponseType1(String id)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-response",
                new XAttribute("id", id),
                new XAttribute("version", OapVersion),
                    new XElement("Status",
                        new XElement("Type", "1"),
                        new XElement("Code", "200")
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String ResponseType2(String id)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-response",
                new XAttribute("id", id),
                new XAttribute("version", OapVersion),
                    new XElement("Status",
                        new XElement("Type", "2"),
                        new XElement("Code", "200")
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String ResponseType3(String id)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-response",
                new XAttribute("id", id),
                new XAttribute("version", OapVersion),
                    new XElement("Status",
                        new XElement("Type", "3"),
                        new XElement("Code", "200")
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String SubscriptionStart(int port, string type)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString())),
                    new XElement("Body",
                    new XAttribute("service", "subscriptionStart"),
                        new XElement("Service", type)
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String SimpleRequest(int port, string callId, string request, string id = "")
        {
            if (id == "")
            {
                id = GenerateXmlId();
            }
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString()),
                        new XElement("To", callId)),
                    new XElement("Body",
                    new XAttribute("service", "OA-XML"),
                        new XElement("service",
                            new XAttribute("version", ServiceVersion),
                            new XElement("Id", id),
                            new XElement(XElement.Parse(request))
                ))));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String SimpleRequestAvailability(int port, string callId, string request, string id = "")
        {
            if (id == "")
            {
                id = GenerateXmlId();
            }
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString()),
                        new XElement("To", callId),
                        new XElement("AvailabilityStatus")),
                    new XElement("Body",
                    new XAttribute("service", "OA-XML"),
                        new XElement("service",
                            new XAttribute("version", ServiceVersion),
                            new XElement("Id", id),
                            new XElement(XElement.Parse(request))
                ))));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String SimpleRequestDelivery(int port, string callId, string request, string id = "")
        {
            if (id == "")
            {
                id = GenerateXmlId();
            }
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString()),
                        new XElement("To", callId),
                        new XElement("DeliveryStatus")),
                    new XElement("Body",
                    new XAttribute("service", "OA-XML"),
                        new XElement("service",
                            new XAttribute("version", ServiceVersion),
                            new XElement("Id", id),
                            new XElement(XElement.Parse(request))
                ))));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String SimpleRequestAvailabilityDelivery(int port, string callId, string request, string id = "")
        {
            if (id == "")
            {
                id = GenerateXmlId();
            }
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString()),
                        new XElement("To", callId),
                        new XElement("AvailabilityStatus"),
                        new XElement("DeliveryStatus")),
                    new XElement("Body",
                    new XAttribute("service", "OA-XML"),
                        new XElement("service",
                            new XAttribute("version", ServiceVersion),
                            new XElement("Id", id),
                            new XElement(XElement.Parse(request))
                ))));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static String RemoveClient(int port)
        {
            XDocument xDoc = new XDocument(
                new XDeclaration("1.0", "UTF-8", null),
                new XElement("OAP-request",
                new XAttribute("id", GenerateXmlId()),
                new XAttribute("version", OapVersion),
                    new XElement("Header",
                        new XElement("Port", port.ToString())),
                    new XElement("Body",
                    new XAttribute("service", "removeClient")
                )));

            StringWriter sw = new Utf8StringWriter();
            xDoc.Save(sw);
            return sw.ToString();
        }

        public static string PrettyXml(string xml)
        {
            String retXml = "";
            try
            {
                var stringBuilder = new StringBuilder();

                var element = XElement.Parse(xml);

                var settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                //settings.NewLineOnAttributes = true;

                using (var xmlWriter = XmlWriter.Create(stringBuilder, settings))
                {
                    element.Save(xmlWriter);
                }

                retXml = stringBuilder.ToString();
            }
            catch (Exception) {
                retXml = xml;
            }
            return retXml;
        }

    }
}
