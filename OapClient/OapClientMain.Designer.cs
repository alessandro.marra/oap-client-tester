﻿namespace OapClient
{
    partial class OapClientMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.labelAddress = new System.Windows.Forms.Label();
            this.txtIp = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.txtLog = new System.Windows.Forms.RichTextBox();
            this.timerSupervision = new System.Windows.Forms.Timer(this.components);
            this.btnSendSubscription = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSupervisionTime = new System.Windows.Forms.TextBox();
            this.comboBoxSubscription = new System.Windows.Forms.ComboBox();
            this.comboBoxRequest = new System.Windows.Forms.ComboBox();
            this.btnSendRequest = new System.Windows.Forms.Button();
            this.txtCallId = new System.Windows.Forms.TextBox();
            this.checkBoxMultipleConnection = new System.Windows.Forms.CheckBox();
            this.btnReConnect = new System.Windows.Forms.Button();
            this.checkBoxSupervision = new System.Windows.Forms.CheckBox();
            this.listBoxStore = new System.Windows.Forms.ListBox();
            this.txtXml = new System.Windows.Forms.RichTextBox();
            this.btnSendFreeRequest = new System.Windows.Forms.Button();
            this.btnStore = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.checkBoxAvailability = new System.Windows.Forms.CheckBox();
            this.checkBoxDelivery = new System.Windows.Forms.CheckBox();
            this.labelListenerPort = new System.Windows.Forms.Label();
            this.txtListenerPort = new System.Windows.Forms.TextBox();
            this.groupBoxFreeMessage = new System.Windows.Forms.GroupBox();
            this.numericUpDownRepetition = new System.Windows.Forms.NumericUpDown();
            this.labelFreeMessageTo = new System.Windows.Forms.Label();
            this.txtFreeMessageCallId = new System.Windows.Forms.TextBox();
            this.labelXml = new System.Windows.Forms.Label();
            this.checkBoxAddClient = new System.Windows.Forms.CheckBox();
            this.labelTo = new System.Windows.Forms.Label();
            this.groupBoxFreeMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepetition)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(200, 11);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(66, 20);
            this.txtPort.TabIndex = 14;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(166, 13);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(29, 13);
            this.labelPort.TabIndex = 13;
            this.labelPort.Text = "Port:";
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Enabled = false;
            this.btnDisconnect.Location = new System.Drawing.Point(744, 7);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnDisconnect.TabIndex = 12;
            this.btnDisconnect.Text = "Stop";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(8, 13);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(48, 13);
            this.labelAddress.TabIndex = 11;
            this.labelAddress.Text = "Address:";
            // 
            // txtIp
            // 
            this.txtIp.Location = new System.Drawing.Point(62, 11);
            this.txtIp.Name = "txtIp";
            this.txtIp.Size = new System.Drawing.Size(100, 20);
            this.txtIp.TabIndex = 10;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(663, 7);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 9;
            this.btnConnect.Text = "Start";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(10, 61);
            this.listBoxLog.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(418, 290);
            this.listBoxLog.TabIndex = 15;
            this.listBoxLog.SelectedIndexChanged += new System.EventHandler(this.listBoxLog_SelectedIndexChanged);
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(436, 35);
            this.txtLog.Margin = new System.Windows.Forms.Padding(2);
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.Size = new System.Drawing.Size(464, 317);
            this.txtLog.TabIndex = 16;
            this.txtLog.Text = "";
            // 
            // timerSupervision
            // 
            this.timerSupervision.Interval = 60000;
            this.timerSupervision.Tick += new System.EventHandler(this.timerSupervision_Tick);
            // 
            // btnSendSubscription
            // 
            this.btnSendSubscription.Enabled = false;
            this.btnSendSubscription.Location = new System.Drawing.Point(255, 355);
            this.btnSendSubscription.Name = "btnSendSubscription";
            this.btnSendSubscription.Size = new System.Drawing.Size(108, 23);
            this.btnSendSubscription.TabIndex = 17;
            this.btnSendSubscription.Text = "Send Subscription";
            this.btnSendSubscription.UseVisualStyleBackColor = true;
            this.btnSendSubscription.Click += new System.EventHandler(this.btnSendSubscription_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(220, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Time (sec)";
            // 
            // txtSupervisionTime
            // 
            this.txtSupervisionTime.Location = new System.Drawing.Point(282, 36);
            this.txtSupervisionTime.Name = "txtSupervisionTime";
            this.txtSupervisionTime.Size = new System.Drawing.Size(37, 20);
            this.txtSupervisionTime.TabIndex = 20;
            this.txtSupervisionTime.Text = "10";
            // 
            // comboBoxSubscription
            // 
            this.comboBoxSubscription.FormattingEnabled = true;
            this.comboBoxSubscription.Items.AddRange(new object[] {
            "User_data",
            "Alarm",
            "Message"});
            this.comboBoxSubscription.Location = new System.Drawing.Point(10, 356);
            this.comboBoxSubscription.Name = "comboBoxSubscription";
            this.comboBoxSubscription.Size = new System.Drawing.Size(236, 21);
            this.comboBoxSubscription.TabIndex = 21;
            // 
            // comboBoxRequest
            // 
            this.comboBoxRequest.FormattingEnabled = true;
            this.comboBoxRequest.Items.AddRange(new object[] {
            "Location_Request",
            "Availability_Query"});
            this.comboBoxRequest.Location = new System.Drawing.Point(436, 355);
            this.comboBoxRequest.Name = "comboBoxRequest";
            this.comboBoxRequest.Size = new System.Drawing.Size(111, 21);
            this.comboBoxRequest.TabIndex = 22;
            // 
            // btnSendRequest
            // 
            this.btnSendRequest.Enabled = false;
            this.btnSendRequest.Location = new System.Drawing.Point(788, 355);
            this.btnSendRequest.Name = "btnSendRequest";
            this.btnSendRequest.Size = new System.Drawing.Size(112, 23);
            this.btnSendRequest.TabIndex = 23;
            this.btnSendRequest.Text = "Send Request";
            this.btnSendRequest.UseVisualStyleBackColor = true;
            this.btnSendRequest.Click += new System.EventHandler(this.btnSendRequest_Click);
            // 
            // txtCallId
            // 
            this.txtCallId.Location = new System.Drawing.Point(620, 356);
            this.txtCallId.Name = "txtCallId";
            this.txtCallId.Size = new System.Drawing.Size(162, 20);
            this.txtCallId.TabIndex = 24;
            // 
            // checkBoxMultipleConnection
            // 
            this.checkBoxMultipleConnection.AutoSize = true;
            this.checkBoxMultipleConnection.Location = new System.Drawing.Point(11, 38);
            this.checkBoxMultipleConnection.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxMultipleConnection.Name = "checkBoxMultipleConnection";
            this.checkBoxMultipleConnection.Size = new System.Drawing.Size(119, 17);
            this.checkBoxMultipleConnection.TabIndex = 25;
            this.checkBoxMultipleConnection.Text = "Multiple Connection";
            this.checkBoxMultipleConnection.UseVisualStyleBackColor = true;
            // 
            // btnReConnect
            // 
            this.btnReConnect.Enabled = false;
            this.btnReConnect.Location = new System.Drawing.Point(825, 7);
            this.btnReConnect.Name = "btnReConnect";
            this.btnReConnect.Size = new System.Drawing.Size(75, 23);
            this.btnReConnect.TabIndex = 26;
            this.btnReConnect.Text = "Re-Start";
            this.btnReConnect.UseVisualStyleBackColor = true;
            this.btnReConnect.Click += new System.EventHandler(this.btnReConnect_Click);
            // 
            // checkBoxSupervision
            // 
            this.checkBoxSupervision.AutoSize = true;
            this.checkBoxSupervision.Checked = true;
            this.checkBoxSupervision.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSupervision.Location = new System.Drawing.Point(134, 38);
            this.checkBoxSupervision.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxSupervision.Name = "checkBoxSupervision";
            this.checkBoxSupervision.Size = new System.Drawing.Size(81, 17);
            this.checkBoxSupervision.TabIndex = 28;
            this.checkBoxSupervision.Text = "Supervision";
            this.checkBoxSupervision.UseVisualStyleBackColor = true;
            // 
            // listBoxStore
            // 
            this.listBoxStore.FormattingEnabled = true;
            this.listBoxStore.Location = new System.Drawing.Point(6, 19);
            this.listBoxStore.Name = "listBoxStore";
            this.listBoxStore.Size = new System.Drawing.Size(230, 238);
            this.listBoxStore.Sorted = true;
            this.listBoxStore.TabIndex = 29;
            this.listBoxStore.SelectedIndexChanged += new System.EventHandler(this.listBoxStore_SelectedIndexChanged);
            // 
            // txtXml
            // 
            this.txtXml.Location = new System.Drawing.Point(280, 19);
            this.txtXml.Name = "txtXml";
            this.txtXml.Size = new System.Drawing.Size(601, 264);
            this.txtXml.TabIndex = 30;
            this.txtXml.Text = "";
            // 
            // btnSendFreeRequest
            // 
            this.btnSendFreeRequest.Enabled = false;
            this.btnSendFreeRequest.Location = new System.Drawing.Point(769, 289);
            this.btnSendFreeRequest.Name = "btnSendFreeRequest";
            this.btnSendFreeRequest.Size = new System.Drawing.Size(112, 23);
            this.btnSendFreeRequest.TabIndex = 31;
            this.btnSendFreeRequest.Text = "Send";
            this.btnSendFreeRequest.UseVisualStyleBackColor = true;
            this.btnSendFreeRequest.Click += new System.EventHandler(this.btnSendFreeRequest_Click);
            // 
            // btnStore
            // 
            this.btnStore.Enabled = false;
            this.btnStore.Location = new System.Drawing.Point(6, 289);
            this.btnStore.Name = "btnStore";
            this.btnStore.Size = new System.Drawing.Size(112, 23);
            this.btnStore.TabIndex = 32;
            this.btnStore.Text = "Save";
            this.btnStore.UseVisualStyleBackColor = true;
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(50, 263);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(186, 20);
            this.txtName.TabIndex = 33;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(6, 266);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(38, 13);
            this.labelName.TabIndex = 34;
            this.labelName.Text = "Name:";
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(124, 289);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(112, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // checkBoxAvailability
            // 
            this.checkBoxAvailability.AutoSize = true;
            this.checkBoxAvailability.Location = new System.Drawing.Point(574, 293);
            this.checkBoxAvailability.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxAvailability.Name = "checkBoxAvailability";
            this.checkBoxAvailability.Size = new System.Drawing.Size(75, 17);
            this.checkBoxAvailability.TabIndex = 36;
            this.checkBoxAvailability.Text = "Availability";
            this.checkBoxAvailability.UseVisualStyleBackColor = true;
            // 
            // checkBoxDelivery
            // 
            this.checkBoxDelivery.AutoSize = true;
            this.checkBoxDelivery.Location = new System.Drawing.Point(653, 293);
            this.checkBoxDelivery.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxDelivery.Name = "checkBoxDelivery";
            this.checkBoxDelivery.Size = new System.Drawing.Size(64, 17);
            this.checkBoxDelivery.TabIndex = 37;
            this.checkBoxDelivery.Text = "Delivery";
            this.checkBoxDelivery.UseVisualStyleBackColor = true;
            // 
            // labelListenerPort
            // 
            this.labelListenerPort.AutoSize = true;
            this.labelListenerPort.Location = new System.Drawing.Point(272, 14);
            this.labelListenerPort.Name = "labelListenerPort";
            this.labelListenerPort.Size = new System.Drawing.Size(69, 13);
            this.labelListenerPort.TabIndex = 38;
            this.labelListenerPort.Text = "Listener Port:";
            // 
            // txtListenerPort
            // 
            this.txtListenerPort.Location = new System.Drawing.Point(347, 11);
            this.txtListenerPort.Name = "txtListenerPort";
            this.txtListenerPort.Size = new System.Drawing.Size(66, 20);
            this.txtListenerPort.TabIndex = 39;
            // 
            // groupBoxFreeMessage
            // 
            this.groupBoxFreeMessage.Controls.Add(this.numericUpDownRepetition);
            this.groupBoxFreeMessage.Controls.Add(this.labelFreeMessageTo);
            this.groupBoxFreeMessage.Controls.Add(this.txtFreeMessageCallId);
            this.groupBoxFreeMessage.Controls.Add(this.labelXml);
            this.groupBoxFreeMessage.Controls.Add(this.listBoxStore);
            this.groupBoxFreeMessage.Controls.Add(this.labelName);
            this.groupBoxFreeMessage.Controls.Add(this.btnSendFreeRequest);
            this.groupBoxFreeMessage.Controls.Add(this.checkBoxDelivery);
            this.groupBoxFreeMessage.Controls.Add(this.txtName);
            this.groupBoxFreeMessage.Controls.Add(this.checkBoxAvailability);
            this.groupBoxFreeMessage.Controls.Add(this.txtXml);
            this.groupBoxFreeMessage.Controls.Add(this.btnStore);
            this.groupBoxFreeMessage.Controls.Add(this.btnDelete);
            this.groupBoxFreeMessage.Location = new System.Drawing.Point(10, 388);
            this.groupBoxFreeMessage.Name = "groupBoxFreeMessage";
            this.groupBoxFreeMessage.Size = new System.Drawing.Size(887, 322);
            this.groupBoxFreeMessage.TabIndex = 40;
            this.groupBoxFreeMessage.TabStop = false;
            this.groupBoxFreeMessage.Text = "Free Message";
            // 
            // numericUpDownRepetition
            // 
            this.numericUpDownRepetition.Location = new System.Drawing.Point(722, 290);
            this.numericUpDownRepetition.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDownRepetition.Name = "numericUpDownRepetition";
            this.numericUpDownRepetition.Size = new System.Drawing.Size(41, 20);
            this.numericUpDownRepetition.TabIndex = 42;
            this.numericUpDownRepetition.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelFreeMessageTo
            // 
            this.labelFreeMessageTo.AutoSize = true;
            this.labelFreeMessageTo.Location = new System.Drawing.Point(277, 294);
            this.labelFreeMessageTo.Name = "labelFreeMessageTo";
            this.labelFreeMessageTo.Size = new System.Drawing.Size(68, 13);
            this.labelFreeMessageTo.TabIndex = 39;
            this.labelFreeMessageTo.Text = "Destinations:";
            // 
            // txtFreeMessageCallId
            // 
            this.txtFreeMessageCallId.Location = new System.Drawing.Point(351, 291);
            this.txtFreeMessageCallId.Name = "txtFreeMessageCallId";
            this.txtFreeMessageCallId.Size = new System.Drawing.Size(208, 20);
            this.txtFreeMessageCallId.TabIndex = 38;
            this.txtFreeMessageCallId.TextChanged += new System.EventHandler(this.txtFreeMessageCallId_TextChanged);
            // 
            // labelXml
            // 
            this.labelXml.AutoSize = true;
            this.labelXml.Location = new System.Drawing.Point(242, 19);
            this.labelXml.Name = "labelXml";
            this.labelXml.Size = new System.Drawing.Size(32, 13);
            this.labelXml.TabIndex = 37;
            this.labelXml.Text = "XML:";
            // 
            // checkBoxAddClient
            // 
            this.checkBoxAddClient.AutoSize = true;
            this.checkBoxAddClient.Checked = true;
            this.checkBoxAddClient.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAddClient.Location = new System.Drawing.Point(339, 38);
            this.checkBoxAddClient.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxAddClient.Name = "checkBoxAddClient";
            this.checkBoxAddClient.Size = new System.Drawing.Size(74, 17);
            this.checkBoxAddClient.TabIndex = 41;
            this.checkBoxAddClient.Text = "Add Client";
            this.checkBoxAddClient.UseVisualStyleBackColor = true;
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Location = new System.Drawing.Point(553, 360);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(63, 13);
            this.labelTo.TabIndex = 42;
            this.labelTo.Text = "Destination:";
            // 
            // OapClientMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 722);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.checkBoxAddClient);
            this.Controls.Add(this.groupBoxFreeMessage);
            this.Controls.Add(this.txtListenerPort);
            this.Controls.Add(this.labelListenerPort);
            this.Controls.Add(this.checkBoxSupervision);
            this.Controls.Add(this.btnReConnect);
            this.Controls.Add(this.checkBoxMultipleConnection);
            this.Controls.Add(this.txtCallId);
            this.Controls.Add(this.btnSendRequest);
            this.Controls.Add(this.comboBoxRequest);
            this.Controls.Add(this.comboBoxSubscription);
            this.Controls.Add(this.txtSupervisionTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSendSubscription);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.txtPort);
            this.Controls.Add(this.labelPort);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.labelAddress);
            this.Controls.Add(this.txtIp);
            this.Controls.Add(this.btnConnect);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OapClientMain";
            this.Text = "OAP Client v1.0.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OapClientMain_FormClosing);
            this.groupBoxFreeMessage.ResumeLayout(false);
            this.groupBoxFreeMessage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepetition)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.TextBox txtIp;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.RichTextBox txtLog;
        private System.Windows.Forms.Timer timerSupervision;
        private System.Windows.Forms.Button btnSendSubscription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSupervisionTime;
        private System.Windows.Forms.ComboBox comboBoxSubscription;
        private System.Windows.Forms.ComboBox comboBoxRequest;
        private System.Windows.Forms.Button btnSendRequest;
        private System.Windows.Forms.TextBox txtCallId;
        private System.Windows.Forms.CheckBox checkBoxMultipleConnection;
        private System.Windows.Forms.Button btnReConnect;
        private System.Windows.Forms.CheckBox checkBoxSupervision;
        private System.Windows.Forms.ListBox listBoxStore;
        private System.Windows.Forms.RichTextBox txtXml;
        private System.Windows.Forms.Button btnSendFreeRequest;
        private System.Windows.Forms.Button btnStore;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.CheckBox checkBoxAvailability;
        private System.Windows.Forms.CheckBox checkBoxDelivery;
        private System.Windows.Forms.Label labelListenerPort;
        private System.Windows.Forms.TextBox txtListenerPort;
        private System.Windows.Forms.GroupBox groupBoxFreeMessage;
        private System.Windows.Forms.Label labelXml;
        private System.Windows.Forms.Label labelFreeMessageTo;
        private System.Windows.Forms.TextBox txtFreeMessageCallId;
        private System.Windows.Forms.CheckBox checkBoxAddClient;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.NumericUpDown numericUpDownRepetition;
    }
}

