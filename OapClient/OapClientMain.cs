﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace OapClient
{
    public partial class OapClientMain : Form
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<String, String> storedRequest;
        String messagesFileName = "messages.json";

        bool isStopped = false;

        public OapClientMain()
        {
            InitializeComponent();
            txtIp.Text = Properties.Settings.Default.OapAddress;
            txtPort.Text = Properties.Settings.Default.OapPort;
            txtListenerPort.Text = Properties.Settings.Default.OapListenerPort;
            checkBoxAddClient.Checked = Properties.Settings.Default.AddClient;
            checkBoxSupervision.Checked = Properties.Settings.Default.Supervision;
            checkBoxMultipleConnection.Checked = Properties.Settings.Default.MultipleConnection;
            txtSupervisionTime.Text = Properties.Settings.Default.SupervisionTime;
            if (File.Exists(messagesFileName))
            {
                using (StreamReader file = File.OpenText(messagesFileName))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    storedRequest = (Dictionary<String, String>)serializer.Deserialize(file, typeof(Dictionary<String, String>));
                }
            }
            //storedRequest = JsonConvert.DeserializeObject<Dictionary<String, String>>(Properties.Settings.Default.StoredRequestDict);
            if (storedRequest == null)
            {
                storedRequest = new Dictionary<String, String>();
            }
            foreach (var item in storedRequest)
            {
                listBoxStore.Items.Add(item.Key);
            }

        }

        class Client
        {
            public TcpClient tcpClient { get; set; }
            public StreamWriter streamWriter { get; set; }
        }
        private Dictionary<String, Client> clients = new Dictionary<String, Client>();
        //private Client selectedClient = new Client();

        private Dictionary<String, TaskCompletionSource<String>> requests = new Dictionary<String, TaskCompletionSource<String>>();
        //private BufferBlock<string> queue = new BufferBlock<string>();

        private TcpClient client;
        private TcpListener listener;

        private int port;

        private int oapPort;
        private IPAddress serverIpAddress;

        private bool multipleConnection;

        private bool connected = false;

        private int reconnectTime = 5000;

        private int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
            //return 1322;
        }

        private async Task Connect(String server, int port)
        {
            IPAddress ipAddress = null;
            UpdateControlStates(false);
            //check if valid ipAddress
            if (IsValidIp(server))
            {
                ipAddress = IPAddress.Parse(server);
            }
            else
            {
                //check the dns name if no ipAddress
                try
                {
                    //IPHostEntry ipHostInfo = Dns.GetHostEntry(server);
                    IPHostEntry ipHostInfo = await Dns.GetHostEntryAsync(server);
                    for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
                    {
                        if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddress = ipHostInfo.AddressList[i];
                            break;
                        }
                    }
                }
                catch { }
            }

            if (ipAddress != null)
            {
                try
                {
                    client = new TcpClient();
                    await client.ConnectAsync(ipAddress, port);
                    HandleResponseAsync();
                }
                catch (Exception e)
                {
                    LogMessage("Error connection to Server: " + e.Message);
                    log.Error("Error connection to Server: " + e.Message);
                    UpdateControlStates(true);
                }
            }
            else
            {
                LogMessage("No Valid Address: " + server);
                log.Error("No Valid Address: " + server);
                UpdateControlStates(true);
            }
        }

        private async Task ConnectMultipleConnection(String server, int port)
        {
            IPAddress ipAddress = null;
            UpdateControlStates(false);
            //check if valid ipAddress
            if (IsValidIp(server))
            {
                ipAddress = IPAddress.Parse(server);
            }
            else
            {
                //check the dns name if no ipAddress
                try
                {
                    //IPHostEntry ipHostInfo = Dns.GetHostEntry(server);
                    IPHostEntry ipHostInfo = await Dns.GetHostEntryAsync(server);
                    for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
                    {
                        if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddress = ipHostInfo.AddressList[i];
                            break;
                        }
                    }
                }
                catch { }
            }

            if (ipAddress != null)
            {
                oapPort = port;
                serverIpAddress = ipAddress;
            }
            else
            {
                LogMessage("No Valid Address: " + server);
                log.Error("No Valid Address: " + server);
                UpdateControlStates(true);
            }
        }

        private bool IsValidIp(string addr)
        {
            IPAddress ip;
            bool valid = !string.IsNullOrEmpty(addr) && IPAddress.TryParse(addr, out ip);
            return valid;
        }

        //persistent connection
        private async void HandleResponseAsync()
        {
            string serverInfo = client.Client.RemoteEndPoint.ToString();
            string clientInfo = client.Client.LocalEndPoint.ToString();
            LogMessage(string.Format("Connected to {0} from {1}", serverInfo, clientInfo));
            log.Info(string.Format("Connected to {0} from {1}", serverInfo, clientInfo));
            connected = true;
            try
            {
                using (var networkStream = client.GetStream())
                using (var reader = new StreamReader(networkStream))
                //using (var writer = new StreamWriter(networkStream))
                {
                    while (true)
                    {
                        char[] buffer = new char[client.ReceiveBufferSize];
                        var received = await reader.ReadAsync(buffer, 0, buffer.Length);

                        char[] recBuf = new char[received];
                        Array.Copy(buffer, recBuf, received);
                        string dataFromServer = new string(recBuf).Trim();

                        if (string.IsNullOrEmpty(dataFromServer))
                        {
                            break;
                        }

                        ReceiveAsync(networkStream, dataFromServer);

                    }
                }
            }
            catch (Exception exp)
            {
                if (!isStopped)
                {
                    LogMessage("Error, Persisten Connection closed: " + exp.Message);
                    log.Error("Error, Persisten Connection closed: " + exp.Message);
                }
                
            }
            finally
            {
                if (!isStopped)
                {
                    LogMessage(string.Format("Closing the connection to {0} from {1}", serverInfo, clientInfo));
                    log.Info(string.Format("Closing the connection to {0} from {1}", serverInfo, clientInfo));
                }
                client.Close();
                UpdateControlStates(true);
                connected = false;
                if (!isStopped)
                {
                    await Task.Delay(reconnectTime);
                    start();
                }
            }
        }

        private async void ReceiveAsync(NetworkStream networkStream, string dataFromServer)
        {

            var lines = dataFromServer.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var sb = new StringBuilder();
            string previousLine = "";
            List<string> xmls = new List<string>();
            bool start = false;

            //find if two xml in one string
            foreach (var line in lines)
            {
                if (line.Contains("version=\"1.0\"") && line.Contains("encoding=\"UTF-8\"") && start)
                {
                    sb.AppendLine(previousLine);
                    xmls.Add(sb.ToString());
                    sb.Clear();
                }
                else
                {
                    sb.AppendLine(previousLine);
                    start = true;
                }
                previousLine = line;
            }
            if (xmls.Count > 0)
            {
                sb.AppendLine(previousLine);
                xmls.Add(sb.ToString());
            }
            else
            {
                xmls.Add(dataFromServer);
            }

            /*
            if (xmls.Count > 1)
            {
                log.Error("xml Splitted!: " + dataFromServer + " / " + string.Join(",", xmls));
            }
            */

            foreach (var xml in xmls)
            {
                string newXml = xml.Trim();
                LogMessage(XmlGenerator.PrettyXml(newXml));
                log.Debug(XmlGenerator.PrettyXml(newXml));

                XDocument xmlResponse = XDocument.Parse(newXml);
                if (xmlResponse.Root.Attribute("id") != null)
                {
                    String id = xmlResponse.Root.Attribute("id").Value.ToLower();
                    if (xmlResponse.Element("OAP-request") != null)
                    {

                        var responseXml = XmlGenerator.ResponseType1(id);
                        //send the response
                        await SendResponse(networkStream, responseXml);
                        //SendResponseAsync(networkStream, responseXml);
                        LogMessage(responseXml);
                        log.Debug(responseXml);
                        //await writer.WriteLineAsync(XmlGenerator.ResponseType1(id));
                        if (xmlResponse.Element("OAP-request").Element("Header") != null)
                        {
                            if (xmlResponse.Element("OAP-request").Element("Header").Element("AvailabilityStatus") != null)
                            {
                                responseXml = XmlGenerator.ResponseType2(id);
                                await SendResponse(networkStream, responseXml);
                                LogMessage(responseXml);
                                log.Debug(responseXml);
                                //await writer.WriteLineAsync(XmlGenerator.ResponseType2(id));
                            }
                            if (xmlResponse.Element("OAP-request").Element("Header").Element("DeliveryStatus") != null)
                            {
                                responseXml = XmlGenerator.ResponseType3(id);
                                await SendResponse(networkStream, responseXml);
                                LogMessage(responseXml);
                                log.Debug(responseXml);
                                //await writer.WriteLineAsync(XmlGenerator.ResponseType3(id));
                            }
                        }
                    }
                    else if (xmlResponse.Element("OAP-response") != null)
                    {
                        if (xmlResponse.Element("OAP-response").Element("Status") != null)
                        {
                            if (xmlResponse.Element("OAP-response").Element("Status").Element("Type") != null)
                            {
                                if (xmlResponse.Element("OAP-response").Element("Status").Element("Type").Value == "1")
                                {
                                    TaskCompletionSource<String> tcs;
                                    if (requests.TryGetValue(id, out tcs))
                                    {
                                        tcs.SetResult(xmlResponse.Element("OAP-response").Element("Status").Element("Code").Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private async void Send(String requestData)
        {
            try
            {
                NetworkStream networkStream = client.GetStream();
                StreamWriter writer = new StreamWriter(networkStream);
                writer.AutoFlush = true;
                //send request
                LogMessage(string.Format("Message sent to Server - {0}", requestData));
                log.Info(string.Format("Message sent to Server - {0}", requestData));
                await writer.WriteLineAsync(requestData);
            }
            catch (Exception e)
            {
                LogMessage("Error connection to Server: " + e.Message);
                log.Error("Error connection to Server: " + e.Message);
            }
        }

        private async Task<bool> SendResponse(NetworkStream networkStream, String requestData)
        {
            bool ok = false;
            try
            {
                StreamWriter writer = new StreamWriter(networkStream);
                writer.AutoFlush = true;
                await writer.WriteLineAsync(requestData);
                ok = true;
            }
            catch (Exception e)
            {
                LogMessage("Error connection to Server: " + e.Message);
            }
            return ok;
        }

        private async Task<String> SendRequest(String requestData, int timeout = 2000)
        {
            String code = "400";
            try
            {
                NetworkStream networkStream = client.GetStream();
                StreamWriter writer = new StreamWriter(networkStream);
                writer.AutoFlush = true;
                //send request
                LogMessage(string.Format("Message sent to Server - {0}", requestData));
                log.Info(string.Format("Message sent to Server - {0}", requestData));
                await writer.WriteLineAsync(requestData);
                //
                XDocument xmlResponse = XDocument.Parse(requestData);
                if (xmlResponse.Root.Attribute("id") != null)
                {
                    String id = xmlResponse.Root.Attribute("id").Value.ToLower();
                    TaskCompletionSource<String> tcs = new TaskCompletionSource<String>();
                    requests.Add(id, tcs);
                    Task timeoutTask = Task.Delay(timeout);
                    Task completedTask = await Task.WhenAny(timeoutTask, tcs.Task);
                    requests.Remove(id);
                    if (completedTask == tcs.Task)
                    {
                        code = tcs.Task.Result;
                    }
                }
            }
            catch (Exception e)
            {
                LogMessage("Error connection to Server: " + e.Message);
                log.Error("Error connection to Server: " + e.Message);
            }
            return code;
        }

        //private async Task SendRequest(String requestData)
        //{
        //    try
        //    {
        //        using (var networkStream = client.GetStream())
        //        using (var reader = new StreamReader(networkStream))
        //        using (var writer = new StreamWriter(networkStream))
        //        {
        //            writer.AutoFlush = true;

        //            LogMessage(string.Format("Message sent to Server: {0}", requestData));

        //            await writer.WriteLineAsync(requestData);

        //            char[] buffer = new char[client.ReceiveBufferSize];
        //            var received = await reader.ReadAsync(buffer, 0, buffer.Length);

        //            char[] recBuf = new char[received];
        //            Array.Copy(buffer, recBuf, received);
        //            string dataFromServer = new string(recBuf).Trim();

        //            LogMessage(string.Format("Message received from Server: {0}", XmlGenerator.PrettyXml(dataFromServer)));

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        LogMessage("Error connection to Server: " + e.Message);
        //    }
        //}

        private async Task SendAndReceive(String requestData)
        {
            try
            {
                //client = new TcpClient();
                TcpClient tcpClient = new TcpClient();
                await tcpClient.ConnectAsync(serverIpAddress, oapPort);
                string clientInfo = tcpClient.Client.LocalEndPoint.ToString() + " -> " + tcpClient.Client.RemoteEndPoint.ToString();
                string id = tcpClient.Client.LocalEndPoint.ToString().Split(':')[1];
                LogMessage(string.Format("{0} - Socket opened - {1}", id, clientInfo));
                log.Info(string.Format("{0} - Socket opened - {1}", id, clientInfo));
                using (var networkStream = tcpClient.GetStream())
                using (var reader = new StreamReader(networkStream))
                using (var writer = new StreamWriter(networkStream))
                {
                    writer.AutoFlush = true;

                    LogMessage(string.Format("{0} - Message sent to Server: {1}", id, requestData));
                    log.Info(string.Format("{0} - Message sent to Server: {1}", id, requestData));

                    await writer.WriteLineAsync(requestData);

                    char[] buffer = new char[tcpClient.ReceiveBufferSize];
                    var received = await reader.ReadAsync(buffer, 0, buffer.Length);

                    char[] recBuf = new char[received];
                    Array.Copy(buffer, recBuf, received);
                    string dataFromServer = new string(recBuf).Trim();

                    LogMessage(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));
                    log.Debug(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));

                    tcpClient.Close();

                    LogMessage(string.Format("{0} - Socket closed - {1}", id, clientInfo));
                    log.Info(string.Format("{0} - Socket closed - {1}", id, clientInfo));
                }
            }
            catch (Exception e)
            {
                LogMessage("Error on sending to Server: " + e.Message);
                log.Error("Error on sending to Server: " + e.Message);
            }
        }

        /*
        private async void SendAndReceiveMulti(string requestData, string id)
        {
            try
            {
                TcpClient tcpClient = new TcpClient();
                await tcpClient.ConnectAsync(serverIpAddress, oapPort);
                string clientInfo = tcpClient.Client.LocalEndPoint.ToString() + " -> " + tcpClient.Client.RemoteEndPoint.ToString();
                //string id = tcpClient.Client.LocalEndPoint.ToString().Split(':')[1];
                LogMessage(string.Format("{0} - Socket opened - {1}", id, clientInfo));
                log.Info(string.Format("{0} - Socket opened - {1}", id, clientInfo));
                using (var networkStream = tcpClient.GetStream())
                using (var reader = new StreamReader(networkStream))
                using (var writer = new StreamWriter(networkStream))
                {
                    writer.AutoFlush = true;

                    LogMessage(string.Format("{0} - Message sent to Server: {1}", id, requestData));
                    log.Info(string.Format("{0} - Message sent to Server: {1}", id, requestData));

                    await writer.WriteLineAsync(requestData);

                    if (requestData.Contains("AvailabilityStatus"))
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            char[] buffer = new char[tcpClient.ReceiveBufferSize];
                            var received = await reader.ReadAsync(buffer, 0, buffer.Length);

                            char[] recBuf = new char[received];
                            Array.Copy(buffer, recBuf, received);
                            string dataFromServer = new string(recBuf).Trim();

                            LogMessage(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));
                            log.Debug(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));
                        }
                    }
                    else
                    {
                        char[] buffer = new char[tcpClient.ReceiveBufferSize];
                        var received = await reader.ReadAsync(buffer, 0, buffer.Length);

                        char[] recBuf = new char[received];
                        Array.Copy(buffer, recBuf, received);
                        string dataFromServer = new string(recBuf).Trim();

                        LogMessage(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));
                        log.Debug(string.Format("{0} - Message received from Server: {1}", id, XmlGenerator.PrettyXml(dataFromServer)));
                    }

                    tcpClient.Close();

                    LogMessage(string.Format("{0} - Socket closed - {1}", id, clientInfo));
                    log.Info(string.Format("{0} - Socket closed - {1}", id, clientInfo));
                }
            }
            catch (Exception e)
            {
                LogMessage("Error on sending to Server: " + e.Message);
                log.Error("Error on sending to Server: " + e.Message);
            }
        }
        */



        private void UpdateControlStates(bool toggle)
        {
            btnConnect.Enabled = toggle;
            btnDisconnect.Enabled = !toggle;
            btnSendSubscription.Enabled = !toggle;
            btnSendRequest.Enabled = !toggle;
            btnReConnect.Enabled = toggle;
        }



        private async void StartListener(int listeningPort)
        {
            listener = new TcpListener(IPAddress.Any, listeningPort);
            listener.Start();
            UpdateControlStates(false);
            LogMessage("Server is running");
            log.Info("Server is running");
            LogMessage("Listening on port " + listeningPort);
            log.Info("Listening on port " + listeningPort);

            while (true)
            {
                LogMessage("Waiting for connections...");
                log.Info("Waiting for connections...");
                try
                {
                    var tcpClient = await listener.AcceptTcpClientAsync();
                    HandleConnectionAsync(tcpClient);
                }
                catch (Exception exp)
                {
                    if (!isStopped)
                    {
                        LogMessage("Error on stopping listener: " + exp.Message);
                        log.Error("Error on stopping listener: " + exp.Message);
                    }
                    break;
                }
            }
        }

        private void StopListener()
        {
            DisconnectAllClients();
            listener.Stop();
            LogMessage("Server stopped");
            log.Info("Server stopped");
            isStopped = false;
            UpdateControlStates(true);
        }

        private async void HandleConnectionAsync(TcpClient tcpClient)
        {
            string serverInfo = tcpClient.Client.RemoteEndPoint.ToString();
            string clientInfo = tcpClient.Client.LocalEndPoint.ToString();
            LogMessage(string.Format("Got connection request from {0} to {1}", serverInfo, clientInfo));
            log.Info(string.Format("Got connection request from {0} to {1}", serverInfo, clientInfo));
            //added by ale
            //Client client = new Client();
            //client.tcpClient = tcpClient;
            //
            try
            {
                using (var networkStream = tcpClient.GetStream())
                using (var reader = new StreamReader(networkStream))
                using (var writer = new StreamWriter(networkStream))
                {
                    writer.AutoFlush = true;
                    //added by ale
                    //client.streamWriter = writer;
                    //clients.Add(clientInfo, client);
                    //listClients.Items.Add(clientInfo);
                    //
                    while (true)
                    {
                        char[] buffer = new char[tcpClient.ReceiveBufferSize];
                        var received = await reader.ReadAsync(buffer, 0, buffer.Length);

                        char[] recBuf = new char[received];
                        Array.Copy(buffer, recBuf, received);
                        string dataFromClient = new string(recBuf).Trim();

                        if (string.IsNullOrEmpty(dataFromClient))
                        {
                            break;
                        }

                        LogMessage(XmlGenerator.PrettyXml(dataFromClient));
                        log.Debug(XmlGenerator.PrettyXml(dataFromClient));

                        XDocument xmlResponse = XDocument.Parse(dataFromClient);
                        if (xmlResponse.Element("OAP-request") != null)
                        {
                            if (xmlResponse.Root.Attribute("id") != null)
                            {
                                var id = xmlResponse.Root.Attribute("id").Value.ToLower();

                                //!!!!!!!!!!!!!!!!!to check
                                var responseXml = XmlGenerator.ResponseType1(id);
                                //send the response
                                await writer.WriteLineAsync(responseXml);
                                LogMessage(responseXml);
                                log.Debug(responseXml);
                                if (xmlResponse.Element("OAP-request").Element("Header") != null)
                                {
                                    if (xmlResponse.Element("OAP-request").Element("Header").Element("AvailabilityStatus") != null)
                                    {
                                        responseXml = XmlGenerator.ResponseType2(id);
                                        //send the response
                                        await writer.WriteLineAsync(responseXml);
                                        LogMessage(responseXml);
                                        log.Debug(responseXml);
                                    }
                                    if (xmlResponse.Element("OAP-request").Element("Header").Element("DeliveryStatus") != null)
                                    {
                                        responseXml = XmlGenerator.ResponseType3(id);
                                        //send the response
                                        await writer.WriteLineAsync(responseXml);
                                        LogMessage(responseXml);
                                        log.Debug(responseXml);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                LogMessage(exp.Message);
                log.Error(exp.Message);
            }
            finally
            {
                LogMessage(string.Format("Closing the client connection - {0}", clientInfo));
                log.Info(string.Format("Closing the client connection - {0}", clientInfo));
                tcpClient.Close();
                //clients.Remove(clientInfo);
            }
        }

        private void DisconnectAllClients()
        {
            foreach (KeyValuePair<String, Client> entry in clients)
            {
                entry.Value.tcpClient.Close();
            }
            clients.Clear();
        }

        private void LogMessage(string message, [CallerMemberName]string callername = "")
        {
            String logEntry = DateTime.Now.ToString("HH:mm:ss") + " - [" + callername + "] - Thread-" + Thread.CurrentThread.ManagedThreadId + " - " + Environment.NewLine + message + "" + Environment.NewLine;
            //if (listBoxLog.InvokeRequired)
            //{
            //    listBoxLog.Invoke(new MethodInvoker(() => { listBoxLog.Items.Add(logEntry); }));
            //}
            //else
            //{
            //    listBoxLog.Items.Add(logEntry);
            //}
            listBoxLog.Items.Add(logEntry);
        }

        private async void start()
        {
            await Connect(txtIp.Text, Convert.ToInt32(txtPort.Text));
            if (checkBoxSupervision.Checked)
            {
                int supervisionTime = Convert.ToInt32(txtSupervisionTime.Text);
                timerSupervision.Interval = (supervisionTime * 1000);
                String code;
                if (checkBoxAddClient.Checked) code = await SendRequest(XmlGenerator.AddClient(port, supervisionTime));
                code = await SendRequest(XmlGenerator.Supervision(port));
                timerSupervision.Start();
            }
         }

        private async void startMultipleConnection()
        {
            await ConnectMultipleConnection(txtIp.Text, Convert.ToInt32(txtPort.Text));
            if (checkBoxSupervision.Checked)
            {
                int supervisionTime = Convert.ToInt32(txtSupervisionTime.Text);
                timerSupervision.Interval = (supervisionTime * 1000);
                if (checkBoxAddClient.Checked) await SendAndReceive(XmlGenerator.AddClient(port, supervisionTime));
                await SendAndReceive(XmlGenerator.Supervision(port));
                timerSupervision.Start();
            }
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.OapAddress = txtIp.Text;
            Properties.Settings.Default.OapPort = txtPort.Text;
            Properties.Settings.Default.OapListenerPort = txtListenerPort.Text;
            Properties.Settings.Default.AddClient = checkBoxAddClient.Checked;
            Properties.Settings.Default.Supervision = checkBoxSupervision.Checked;
            Properties.Settings.Default.MultipleConnection = checkBoxMultipleConnection.Checked;
            Properties.Settings.Default.SupervisionTime = txtSupervisionTime.Text;
            Properties.Settings.Default.Save();

            if (txtListenerPort.Text == "") port = FreeTcpPort();
            else port = Convert.ToInt32(txtListenerPort.Text);
            StartListener(port);
            multipleConnection = checkBoxMultipleConnection.Checked;
            if (!multipleConnection) start();
            else startMultipleConnection();
        }

        private async void btnDisconnect_Click(object sender, EventArgs e)
        {
            isStopped = true;
            try
            {
                timerSupervision.Stop();
                if (!multipleConnection)
                {
                    if (checkBoxAddClient.Checked) await SendRequest(XmlGenerator.RemoveClient(port));
                    client.Close();
                }
                else
                {
                    if (checkBoxAddClient.Checked) await SendAndReceive(XmlGenerator.RemoveClient(port));
                }
                StopListener();
            }
            catch (Exception) { }
            connected = false;
        }

        private void listBoxLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtLog.Text = listBoxLog.SelectedItem.ToString();
        }

        private async void timerSupervision_Tick(object sender, EventArgs e)
        {
            if (!multipleConnection) await SendRequest(XmlGenerator.Supervision(port));
            else await SendAndReceive(XmlGenerator.Supervision(port));
        }

        private async void btnSendSubscription_Click(object sender, EventArgs e)
        {
            var subscriptionType = comboBoxSubscription.Text;
            if (!multipleConnection) await SendRequest(XmlGenerator.SubscriptionStart(port, subscriptionType));
            else await SendAndReceive(XmlGenerator.SubscriptionStart(port, subscriptionType));
        }

        private async void btnSendRequest_Click(object sender, EventArgs e)
        {
            var request = "<" + comboBoxRequest.Text + "/>";
            var callId = txtCallId.Text;
            if (request != "" && callId != "")
            {
                if (!multipleConnection) await SendRequest(XmlGenerator.SimpleRequest(port, callId, request));
                else await SendAndReceive(XmlGenerator.SimpleRequest(port, callId, request));
            }
                
            //else
            //{
            //    for (int i = 0; i < 5; i++)
            //    {
            //        SendAndReceiveMulti(XmlGenerator.SimpleRequestAvailabilityDelivery(port, callId, request), i.ToString());
            //    }
            //}

        }

        private void btnReConnect_Click(object sender, EventArgs e)
        {
            StartListener(port);
            multipleConnection = checkBoxMultipleConnection.Checked;
            if (!multipleConnection) start();
            else startMultipleConnection();
        }

        private async void OapClientMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                timerSupervision.Stop();
                if (!multipleConnection)
                {
                    if (checkBoxAddClient.Checked) await SendRequest(XmlGenerator.RemoveClient(port));
                    client.Close();
                }
                else
                {
                    if (checkBoxAddClient.Checked) await SendAndReceive(XmlGenerator.RemoveClient(port));
                }
                StopListener();
            }
            catch (Exception) { }
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            String xml = XmlGenerator.PrettyXml(txtXml.Text);
            if (xml != "")
            {
                if (!listBoxStore.Items.Contains(txtName.Text))
                {
                    listBoxStore.Items.Add(txtName.Text);
                    storedRequest.Add(txtName.Text, xml);
                }
                else
                {
                    storedRequest[txtName.Text] = xml;
                }
                txtName.Text = "";
                txtXml.Text = "";
                using (StreamWriter file = File.CreateText("messages.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, storedRequest);
                }

                //Properties.Settings.Default.StoredRequestDict = JsonConvert.SerializeObject(storedRequest);
                //Properties.Settings.Default.Save();
            }
            else
            {
                MessageBox.Show("Invalid XML String entered");
            }
        }

        private void listBoxStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = listBoxStore.SelectedItem.ToString();
                txtXml.Text = storedRequest[listBoxStore.SelectedItem.ToString()];
            }
            catch (Exception) { }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (listBoxStore.Items.Contains(txtName.Text))
            {
                listBoxStore.Items.Remove(txtName.Text);
                storedRequest.Remove(txtName.Text);
                using (StreamWriter file = File.CreateText("messages.json"))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, storedRequest);
                }
                //Properties.Settings.Default.StoredRequestDict = JsonConvert.SerializeObject(storedRequest);
                //Properties.Settings.Default.Save();
            }
            txtName.Text = "";
            txtXml.Text = "";
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            Boolean isTextBoxEmpty = string.IsNullOrEmpty(txtName.Text);
            btnStore.Enabled = !isTextBoxEmpty;
            btnDelete.Enabled = !isTextBoxEmpty;
        }

        private async void btnSendFreeRequest_Click(object sender, EventArgs e)
        {
            string id = XmlGenerator.GenerateXmlId();
            for (int i = 0; i < numericUpDownRepetition.Value; i++)
            {
                var callIds = txtFreeMessageCallId.Text.Split(',');
                foreach (var callId in callIds)
                {
                    var request = txtXml.Text;
                    //var callId = txtFreeMessageCallId.Text;
                    if (request != "" && callId != "")
                    {
                        request = request.Replace("%c", (i + 1).ToString());
                        if (!multipleConnection)
                        {
                            if (checkBoxAvailability.Checked && checkBoxDelivery.Checked) await SendRequest(XmlGenerator.SimpleRequestAvailabilityDelivery(port, callId, request, id));
                            else if (checkBoxAvailability.Checked) await SendRequest(XmlGenerator.SimpleRequestAvailability(port, callId, request, id));
                            else if (checkBoxDelivery.Checked) await SendRequest(XmlGenerator.SimpleRequestDelivery(port, callId, request, id));
                            else await SendRequest(XmlGenerator.SimpleRequest(port, callId, request, id));
                        }
                        else
                        {
                            if (checkBoxAvailability.Checked && checkBoxDelivery.Checked) await SendAndReceive(XmlGenerator.SimpleRequestAvailabilityDelivery(port, callId, request, id));
                            else if (checkBoxAvailability.Checked) await SendAndReceive(XmlGenerator.SimpleRequestAvailability(port, callId, request, id));
                            else if (checkBoxDelivery.Checked) await SendAndReceive(XmlGenerator.SimpleRequestDelivery(port, callId, request, id));
                            else await SendAndReceive(XmlGenerator.SimpleRequest(port, callId, request, id));
                        }
                    }
                }
            }
        }

        private void txtFreeMessageCallId_TextChanged(object sender, EventArgs e)
        {
            Boolean isTextBoxEmpty = string.IsNullOrEmpty(txtFreeMessageCallId.Text);
            if (!isTextBoxEmpty && checkBoxMultipleConnection.Checked)
            {
                btnSendFreeRequest.Enabled = true;
            }
            else if (!isTextBoxEmpty && !checkBoxMultipleConnection.Checked && connected)
            {
                btnSendFreeRequest.Enabled = true;
            }
            else
            {
                btnSendFreeRequest.Enabled = false;
            }
        }

    }
}
